# Securisation of Linux server

***

## User management

By default, the **root** user will be present on the server. Hwever, it is important to create a second user  that will not be **root**, because if an attack succeed, the attacker will be connected with an other root account.

#### Create user

The linux command to create an user is *adduser*.

```bash
adduser UserName
```

Once this has been created, think to edit the user password with the command *passwd*.  This must be done from the **root** account.

```bash
passwd UserName
```

***

## SSH

By default, the SSH protocol is open on the server. The identification is made with password. To improve security, we are going to implement a key authentication with the RSA encryption protocol. 

### Key generation

To generate a pair of RSA key on the **client**, we are going to use the command *ssh-keygen*.

```bash
ssh-keygen -t rsa -b 4096
```

*Options* :

- **t** : To select the key type (RSA, DSA, etc...).

- **b** : To select the key size.

Two keys are going generate, one public and the other private.

```bash
YourKey YourKey.pub
```

- **Without extension** : Private key.

- **.pub** : Public key.

### Key sharing

The public key must be transmitted to the server, to which we wish to establish our SSH connection by key. We are going to use the commande *ssh-copy-id*.

```bash
ssh-copy-id -i VotreCle.pub UtilisateurServeur@IPServeur
```

*Options* :

- **i** : permet de séléctionner la clé souhaité.

### Automatisation de la connexion

Nous allons mettre en place un script permettant la connexion automatique au serveur.

La première étape est de créer l'agent de gestion de clé grâce à la commande *ssh-agent*.

```bash
eval `ssh-agent`
```

*Description* :

- **eval** : Permet d'exécuter la comande suivante.

- \` VotreCommande \` : Permet d'éxécuter ce qui est contenu dans les apostrophes

Cette ligne appelle donc la commande *ssh-agent* et exécute sa sortie, ce qui a pour but de généré notre agent.

***/!\  L'agent n'existe que dans l'instance du terminal. Si vous ouvrez un nouveau terminal, il faudra à nouveau effectuer la commande.***

La deuxième étape est d'enregistrer la clé dans l'agent. Pour cela nous utiliserons la commande *ssh-add*.

```bash
ssh-add VotreRepertoire/ClePrivee
```

Si tout a bien fonctionné, vous pouvez vous connecter au serveur grâce à votre clé. 

```bash
ssh UtilisateurServeur@IPServeur
```

### Configuration de SSH

Maintenant, nous allons modifier le fichier  */etc/ssh/sshd_config* gérant les paramètres SSH sur le serveur. 

```bash
PubkeyAuthentication yes
```

**PubkeyAuthentication** : Permet de se connecter au serveur en SSH via une clé (configurer précédemment).

```bash
PasswordAuthentication no
```

**PasswordAuthentication** : Empêche de se connecter au serveur en SSH via mot de passe.

***/!\ À réaliser une fois la connexion par clé fonctionnelle.***

```bash
PermitEmptyPasswords no
```

**PermitEmptyPasswords** : Permet d'empêcher l'utilisation de mot de passe vide. Même si la connexion par mot de passe sera normalement désactivée, cela apporte une protection supplémentaire.

```bash
MaxAuthTries 3
```

**MaxAuthTries** : Permet de bloquer l'IP tentant de se connecter au bout d'un certains nombre d'erreur de connexion.

```bash
MaxSessions 5
```

**MaxSessions** : Permet de limiter le nombre maximum de session SSH ouvertes en même temps.

```bash
StrictModes yes
```

**StrictModes** : Permet de vérifier les modes et le propriétaire des fichiers de l'utilisateur et du répertoire de base (home directory) de l'utilisateur avant d'accepter une
connexion. C'est normalement souhaitable, car si mal configuré, des répertoires ou des fichiers en peuvent être en accès complet.

```bash
X11Forwarding no
```

**X11Forwarding** : Le transfert X11 est une méthode permettant à un utilisateur de lancer une application graphique installée sur un système Linux distant et de transférer les fenêtres de cette application (écran) vers le système local. Il est donc plus sécurisé de ne pas laisser cette possibilitée disponible.

***

## Pare-feu

/* iptable

***

## Fail2Ban

***

## Routeur

/* parler de la redirection du port extérieur
